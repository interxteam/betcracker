<?php
namespace Library;

class ValidationsErrorsException extends \Exception
{
    protected $errors = array();

    public function __construct($name = '', $message ='')
    {
        if(!empty($name) && !empty($message))
            $this->dodaj($name, $message);
    }

    public function add($name,$message)
    {
        $this->errors[$name] = $message;
    }

    public function get($name)
    {
            return $this->errors[$name];
    }

    public function getAll()
    {
        return $this->errors;
    }

    public function hasErrors()
    {
        return count($this->errors) > 0;
    }
}

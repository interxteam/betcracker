<?php


namespace Library;

class Validator
{
	public static function isUrl($string)
        {
            return filter_var($string, FILTER_VALIDATE_URL) ? true : false;
//            if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$website)) {
//              $websiteErr = "Invalid URL";
//            }
        }
		
	public static function isNull($var)
	{
		return is_null($var);
	}
    
	public static function isBase64($string)
	{
		return preg_match('#^[a-zA-Z\d/+]*={0,2}$#', $string) ? true : false;
	}

	public static function isSaveString($string)
	{
		$nieDozwoloneZnaki = array('<','>','\\',"'",'"','`','#',"$",'@','+','%');
		for($i=0;$i<9;$i++)
		{
			if(strpos($string,$nieDozwoloneZnaki[$i])!==false){return false;}
		}
		return true;
	}

	public static function isCellPhone($numer)
	{
//		return preg_match('/\+?([0-9]+){9,11}/i');// cos wadliwie dzialalo
		$numer = str_replace(array(' ','-','(',')','+'),'',$numer);
		return is_numeric($numer) && strlen($numer)>=7;
	}

	public static function isDate($data)
	{
        $date = explode('-', $data);
		if(!isset($date[0]))
			return false;
		if(!isset($date[1]))
			return false;
		if(!isset($date[2]))
			return false;
        if(!checkdate($date[1], $date[2], $date[0])) return false;
		return preg_match('/^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}$/',$data);
	}

	public static function isDate2($data)
	{
		// echo $data;
		return preg_match('/^[0-9]{1,2}-[0-9]{1,2}-[0-9]{4}$/',$data);
	}

	public function isTime($godzina)
	{
		if(preg_match('/^([0-9]{1,2}):([0-9]{1,2})$/', $godzina, $tab))
		{
			if(0 < $tab[0] && $tab[0] <=23 && 0 < $tab[1] && $tab[1] < 60)
				return true;
		}
		if(preg_match('/^([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})$/', $godzina, $tab))
		{
			if(0 < $tab[0] && $tab[0] <=23 && 0 < $tab[1] && $tab[1] < 60 && 0 < $tab[2] && $tab[2] < 60)
				return true;
		}
		return false;
	}

	public static function isDateTime(&$dataTime)
	{
		return preg_match('/^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2} [0-9]{1,2}:[0-9]{1,2}:[0-9]{1,2}$/',$dataTime);
	}

	public static function isNumber(&$nr)
	{
		return is_numeric($nr);
	}

	public static function isPostCode(&$kod)
	{
		return @ereg('^([0-9]{2})-([0-9]{3})$',$kod) > 0;
	}

	public static function isEmail($email)
	{
		return preg_match("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$^", $email);
	}

	public static function isPesel($p)
	{
		if(empty($p))
			return false;
		if(!Walidator::toNumer($p))
			return false;
		$wagi = array(1, 3, 7, 9, 1, 3, 7, 9, 1, 3);
		$sum = 0;

		for($i = 0; $i < 10; $i++) {

			$sum += $wagi[$i] * substr($p, $i, 1);

		}

		$m = $sum % 10;

		if($m > 0)
			$lk = 10 - $m;
		else
			$lk = 0;

		return substr($p, 10, 1) == $lk;
	}

	public static function isTablica(&$tab, $mozeBycPusta=true)
	{
		if(!is_array($tab))return false;
		if($mozeBycPusta)
			return true;
		return count($tab)>0;
	}

	public function isBool( $zmienna )
	{
		return is_bool( $zmienna );
	}

	public static function isCharsazAZ09(&$ciag)
	{
		return @ereg('^([a-zA-Z0-9]+)$',$ciag) > 0;
	}

	public static function isRightLength(&$string, $min=null,$max=null)
	{
		$w = true;
		if(!is_null($min))
			$w = $min <= strlen($string);
		if(!is_null($max))
			$w = strlen($string) <= $max;

		return $w;
	}
}
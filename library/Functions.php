<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Library;

class Functions
{

    public static function trueIp()
    {
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        else
            return $_SERVER['REMOTE_ADDR'];
    }
	
	public static function date($format, $timestamp)
	{
		return iconv("Windows-1250","UTF-8",ucfirst(strftime($format,$timestamp)));
	}
	
	//wycina tekst i zwraca go
	public static function tnijTekst($tekst,$odTekstu,$doTekstu, $indexOstatniegoZnaku = false)
	{
		if( $indexOstatniegoZnaku === false )
			$indexOstatniegoZnaku = strpos($tekst,$odTekstu);
		else
			$indexOstatniegoZnaku = strpos($tekst,$odTekstu, $indexOstatniegoZnaku);

		if($indexOstatniegoZnaku === false) return '';

		$indexOstatniegoZnaku += strlen($odTekstu);
		return substr($tekst, $indexOstatniegoZnaku, strpos($tekst,$doTekstu,$indexOstatniegoZnaku) - $indexOstatniegoZnaku);
	}
	// zwraca tablic�
	public static function tnijTekstNaTablice($tekst,$odTekstu,$doTekstu,$prefixElementu = '', $sufixElementu = '')
	{
		$w = array();
		while(true)
		{
			$indexOstatniegoZnaku = strpos($tekst,$odTekstu);
			if($indexOstatniegoZnaku === false) return $w;
			$indexOstatniegoZnaku += strlen($odTekstu);
			$w [] = $prefixElementu.substr($tekst, $indexOstatniegoZnaku, strpos($tekst,$doTekstu,$indexOstatniegoZnaku) - $indexOstatniegoZnaku).$sufixElementu;
	//		$tekst = substr($tekst, strpos($tekst,$doTekstu,$indexOstatniegoZnaku) - $indexOstatniegoZnaku + 1);
			$tekst = substr($tekst, strpos($tekst,$doTekstu,$indexOstatniegoZnaku) + strlen($doTekstu));

	//		var_dump($tekst);
		}
	}


	//wycina od tekstu do tekstu i zwraca tekst po wyci�ciu z niego partji
	public static function wytnijTekst($tekst, $odTekstu, $doTekstu, $wstawWMiejsceWyciecia='')
	{
		$indexPierwszegoZnaku = strpos($tekst,$odTekstu);
		if($indexPierwszegoZnaku === false) return $tekst;
		return substr($tekst, 0, $indexPierwszegoZnaku) . $wstawWMiejsceWyciecia . substr($tekst, strpos($tekst,$doTekstu,$indexPierwszegoZnaku) + strlen($doTekstu));
	}
}

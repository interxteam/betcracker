<?php
namespace Models;

/**
 * Description of kursy_1x2
 *
 * @author InterX-PC
 */
class Kursy1x2 {
	
	const tabela = 'kursy_1x2';
	
	protected 
		$db, 
		$idMeczu = null, 
		$id = null
	;
	
	public function __construct(\Library\Database\PdoWrapper $db) {
		$this->db = $db;
	}
	
	public function dodaj($stawka1, $stawkaX, $stawka2) {
		if(\Library\Validator::isNull($this->idMeczu))
			return false;
		if(!\Library\Validator::isNumber($stawka1))
			return false;
		if(!\Library\Validator::isNumber($stawkaX))
			return false;
		if(!\Library\Validator::isNumber($stawka2))
			return false;
		
		//spr czy nie istnieje
		$czyZnalazl = $this->db->selectFirst(self::tabela,[
			'id_meczu' => $this->idMeczu,
		]);
		
		if(isset($czyZnalazl['id'])){
			return $this->id = $czyZnalazl['id'];
		}
			
		$this->id = $this->db->insert(self::tabela,[
			'id_meczu' => $this->idMeczu,
			'gospodarze' => $stawka1,
			'remis' => $stawkaX,
			'goscie' => $stawka2,
		]);
		
		return $this->id;
	}
	
	public function sprawdzWygrana(){
		
		if(!\Library\Validator::isNull($this->idMeczu)) {
			$info = $this->db->selectFirst(self::tabela,[
				'id_meczu' => $this->idMeczu,
			]);
			
		} elseif( !\Library\Validator::isNull($this->id) ) {
			$info = $this->db->selectFirst(self::tabela,[
				'id' => $this->id,
			]);
		} else {
			return false;
		}
		
		if(!isset($info['id']))
			return false;
		
		$mecz = $this->db->selectFirst(\Models\Mecze::tabela,[
			'id' => $info['id_meczu'],
			'status' => 'zakonczono',
		]);
		
		list($goleGospodarzy, $goleGosci) = explode(':', $mecz['wynik']);
		
		if($goleGospodarzy == '-')
			return false;
		
		if( (int)$goleGospodarzy > (int)$goleGosci){
			
			$wygrana = 'gospodarze';
			
		} elseif ((int)$goleGospodarzy == (int)$goleGosci) {
			$wygrana = 'remis';
			
		} else {
			$wygrana = 'goscie';
		}
			
		$this->db->update(self::tabela, [
			'wygrana' => $wygrana,
		], ['id' => $info['id']]);
		
		return true;
	}
	
	public function pobierzDaneDlaMeczu() {
		if(\Library\Validator::isNull($this->idMeczu))
			return false;
		
		return $this->db->selectFirst(self::tabela,['id_meczu' => $this->idMeczu]);
	}


	public function ustawIdMeczu($id) {
		if(!\Library\Validator::isNumber($id))
			return false;
		$this->idMeczu = $id;
		return true;
	}
	
	public function ustawId($id) {
		if(!\Library\Validator::isNumber($id))
			return false;
		$this->id = $id;
		return true;
	}
}

<?php

namespace Models;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of urlsBuffor
 *
 * @author InterX-PC
 */
class UrlsBuffor
{

	protected $path = '';

	public function __construct($path)
	{
		$this->path = $path;
	}

	public function getContent($url, $folder = null, $referer = '')
	{
		if (is_null($folder)) {
			$newFilenameWithPath = $this->path . 'text_' . substr(str_replace(['/', '\\', ':', '?', '&', ';', '=', '%'], '-', $url), 7) . '.txt';
		} else {
			
			if(!is_dir($this->path.$folder.'/')) {
				mkdir($this->path.$folder.'/', 0777);
				chmod($this->path.$folder.'/', 0777);
			}
				
			$newFilenameWithPath = $this->path . $folder . '/text_' . substr(str_replace(['/', '\\', ':', '?', '&', ';', '=', '%'], '-', $url), 7) . '.txt';
			$oldFilenameWithPath = $this->path . 'text_' . substr(str_replace(['/', '\\', ':', '?', '&', ';', '=', '%'], '-', $url), 7) . '.txt';

			if (file_exists($oldFilenameWithPath)) {
				rename($oldFilenameWithPath, $newFilenameWithPath);
			}
		}

		if (file_exists($newFilenameWithPath)) {
			$html = file_get_contents($newFilenameWithPath);
		} else {
//			var_dump($this->path.'text_'.substr(str_replace(['/','\\',':','?','&',';','=','%'], '-', $url),7).'.txt');
//			echo 'pobieranie z internetu';
//			exit();
			list($html,$header) = $this->get_fcontent($url, $referer);
//			var_dump($html);
//			exit();
//			file_put_contents($newFilenameWithPath, $html);
		}
		return $html;
	}
	
	private function get_fcontent( $url,   $refererUrl = '') 
	{
		$url = str_replace( "&amp;", "&", urldecode(trim($url)) );

		$cookie = tempnam ("/tmp", "CURLCOOKIE");
		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1" );
		curl_setopt( $ch, CURLOPT_URL, $url );
		if(!empty($refererUrl))
		{
			curl_setopt( $ch, CURLOPT_REFERER, $refererUrl);
		}
		curl_setopt( $ch, CURLOPT_COOKIEJAR, $cookie );
		curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
		curl_setopt( $ch, CURLOPT_ENCODING, "" );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_AUTOREFERER, true );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );    # required for https urls
		curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 5 );
		curl_setopt( $ch, CURLOPT_TIMEOUT, 5 );
		curl_setopt( $ch, CURLOPT_MAXREDIRS, 10 );
		$content = curl_exec( $ch );
		$response = curl_getinfo( $ch );
		curl_close ( $ch );

		if ($response['http_code'] == 301 || $response['http_code'] == 302) {
			ini_set("user_agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1");

			if ( $headers = get_headers($response['url']) ) {
				foreach( $headers as $value ) {
					if ( substr( strtolower($value), 0, 9 ) == "location:" )
						return get_url( trim( substr( $value, 9, strlen($value) ) ) );
				}
			}
		}
			return array( $content, $response );
	}


}

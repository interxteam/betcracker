<?php

namespace Models;

class Mecze
{
	const tabela = 'mecz';
	protected 
		$db, 
		$id = null,
		$idLigi = null, 
		$idSezonu = null, 
		$status = 'nierozpoczeto', 
		$statusy = [
			'zakonczono' => [],
			'przerwa' => [],
			'nierozpoczeto' => [],
			'trwa' => [],
		]
	;


	public function __construct(\Library\Database\PdoWrapper $db, $id = null) {
		$this->db = $db;
		
		if(!is_null($id)) {
//			$this->setId($id);
		}
	}
	
	public function ustawStatus($status)
	{
		if( !isset($this->statusy[$status]) )
			return false;
		$this->status = $status;
		return true;
	}
	
	public function pobierzDanePrzezIdStatystyk($id) {
		$this->db->query('SELECT * FROM mecz WHERE id_stats=\''.$statystykiMeczy[$i]['id'].'\'');
		if( $this->db->num_rows() > 0) {
			return $this->db->fetch_assoc();
		}
		return false;
	}
	
	public function pobierzDane() {
		if( \Library\Validator::isNull($this->id))
			return FALSE;
	}
	
	public function pobierzWszystkoDlaSezonu($idSezonu) {
		if(!\Library\Validator::isNumber($idSezonu))
			return false;
		
		$mecze = $this->db->query('
			SELECT 
				t1.*,
				t2.nazwa as gospodarze,
				t3.nazwa as goscie
			FROM 
				'.self::tabela.' t1 
			INNER JOIN 
				'.Druzyny::tabela.' t2
			ON
			(
				t1.id_gospodarzy = t2.id 
			)
			INNER JOIN 
				'.Druzyny::tabela.' t3
			ON
			(
				t1.id_gosci = t3.id 
			)
			WHERE 
				id_sezonu=:idSezonu 
			ORDER BY 
				`data` DESC
		',['idSezonu'=>$idSezonu]);
		
		
		$obKursy1x2 = new Kursy1x2($this->db);
		$obKursyDc = new KursyDc($this->db);
		$obKursyHa = new KursyHa($this->db);
		$obKursyOu = new KursyOu($this->db);
		for($i=0,$lenI=count($mecze);$i<$lenI;$i++) {
			$obKursy1x2->ustawIdMeczu($mecze[$i]['id']);
			$obKursyDc->ustawIdMeczu($mecze[$i]['id']);
			$obKursyHa->ustawIdMeczu($mecze[$i]['id']);
			$obKursyOu->ustawIdMeczu($mecze[$i]['id']);
			$mecze[$i]['kursy1x2'] = $obKursy1x2->pobierzDaneDlaMeczu();
			$mecze[$i]['kursyDc'] = $obKursyDc->pobierzDaneDlaMeczu();
			$mecze[$i]['kursyHa'] = $obKursyHa->pobierzDaneDlaMeczu();
			$mecze[$i]['kursyOu'] = $obKursyOu->pobierzDaneDlaMeczu();
		}
//		print_r($mecze[0]);
//		exit();
		return $mecze;
	}
			
			

	public function dodaj(Druzyny $obGospodarze, Druzyny $obGoscie, $data, $wynik = '-:-', $wynikDoPolowy='-:-', $godzinaRozpoczecia = '00:00:00', $idStatystyk = null, $wynikDogrywki = null, $wynikKarnych = null)
	{
		if( !\Library\Validator::isDate($data) )
			return false;
		
		$czyIstniejeMecz = $this->db->selectFirst(self::tabela,[
			'id_gosci' => $obGoscie->pobierzId(),
			'id_gospodarzy' => $obGospodarze->pobierzId(),
			'data' => $data,
		]);
		
		if(isset($czyIstniejeMecz['id'])) {
			$this->id = $czyIstniejeMecz['id'];
			return $this->id;
		}
		
		
		return $this->db->insert(self::tabela,[
			'id_ligi' => $this->idLigi,
			'id_sezonu' => $this->idSezonu,
			'id_gosci' => $obGoscie->pobierzId(),
			'id_gospodarzy' => $obGospodarze->pobierzId(),
			'data' => $data,
			'godzina_rozpoczecia' => $godzinaRozpoczecia,
			'wynik' => $wynik,
			'wynik_do_polowy' => $wynikDoPolowy,
			'status' => $this->status,
			'id_stats' => $idStatystyk,
			'dogrywka' => $wynikDogrywki,
			'karniaki' => $wynikKarnych,
		]);
	}
	
	public function zmienIdSezonu($id)
	{
		if(is_null($this->id))
			return false;
		if(!\Library\Validator::isNumber($id))
			return false;
		
		$this->idSezonu = $id;
		return $this->db->update(self::tabela, ['id_sezonu'=>$id],['id'=>  $this->id]);
	}
	
	public function ustawIdLigi($id)
	{
		if(!\Library\Validator::isNumber($id))
			return false;
		$this->idLigi = $id;
		return true;
	}
	
	public function ustawIdSezonu($id)
	{
		if(!\Library\Validator::isNumber($id))
			return false;
		$this->idSezonu = $id;
		return true;
	}
	
	public function ustawId($id)
	{
		if(!\Library\Validator::isNumber($id))
			return false;
		$this->id = $id;
		return true;
	}
}
<?php

namespace Models;

class Sezony
{

	const tabela = 'sezony';

	protected
			$db,
			$idLigi = null,
			$id = null

	;

	public function __construct(\Library\Database\PdoWrapper $db)
	{
		$this->db = $db;
	}

	public function dodaj($od, $do = '', $url = '')
	{

		$od = date('Y-m-d', strtotime($od));
		if (\Library\Validator::isNull($this->idLigi))
			throw new Exception(__CLASS__ . '->' . __METHOD__ . ': brak ustawienia idLigi');
		if (!\Library\Validator::isDate($od))
			throw new Exception(__CLASS__ . '->' . __METHOD__ . ': to nie data "' . $od . '"');

		if (!empty($do))
			$do = date('Y-m-d', strtotime($do));

		if (!empty($do) && !\Library\Validator::isDate($do))
			throw new Exception(__CLASS__ . '->' . __METHOD__ . ': to nie data "' . $do . '"');
		if (!empty($url) && !\Library\Validator::isUrl($url))
			throw new Exception(__CLASS__ . '->' . __METHOD__ . ': to nie url "' . $url . '"');

		$ostatniRok = (int) substr($od, 0, 4);
		$ostatniRok++;

		$czyZnalazl = $this->db->selectFirst(self::tabela, [
			'id_ligi' => $this->idLigi,
			'data_od' => $od,
		]);

		if (isset($czyZnalazl['id'])) {
			return $this->id = $czyZnalazl['id'];
		}

		$this->id = $this->db->insert(self::tabela, [
			'id_ligi' => $this->idLigi,
			'data_od' => $od,
			'data_do' => empty($do) ? null : $do,
			'ostatni_rok' => $ostatniRok,
			'url_stats' => empty($url) ? null : $url,
		]);

		return $this->id;
	}

	public function pobierzDane()
	{
		if (\Library\Validator::isNull($this->id))
			return false;
		return $this->db->selectFirst(self::tabela, ['id' => $this->id]);
	}

	public function pobierzAbsolutnieWszystko()
	{
		return $this->db->select(self::tabela);
	}

	public function pobierzWszystko()
	{
		return $this->db->select(self::tabela, ['id_ligi' => $this->idLigi], null, null, ['data_od' => 'DESC']);
	}

	public function pobierzWszystkoZNiepobranymiStatystykami()
	{
		return $this->db->select(self::tabela, ['zakonczone_pobieranie' => 0], null, null, ['data_od' => 'DESC']);
	}

	public function pobierzRozgrywane()
	{
		return $this->db->query('SELECT t1.* FROM `' . self::tabela . '` t1 JOIN (SELECT max(ostatni_rok), id, id_ligi FROM `' . self::tabela . '` group by id_ligi) t2 ON (t1.id = t2.id) ');
	}

	public function zmienUrl($url)
	{
		if (\Library\Validator::isNull($this->id))
			return false;
		if (!\Library\Validator::isUrl($url))
			return false;


		$this->db->update(self::tabela, ['url_stats' => $url], ['id' => $this->id]);

		return true;
	}

	public function oznaczJakoPobrany()
	{
		if (\Library\Validator::isNull($this->id))
			return false;

		$this->db->update(self::tabela, ['zakonczone_pobieranie' => 1], ['id' => $this->id]);

		return true;
	}

	public function ustawIdLigi($id)
	{
		if (!\Library\Validator::isNumber($id))
			return false;
		$this->idLigi = $id;
		return true;
	}

	public function ustawId($id)
	{
		if (!\Library\Validator::isNumber($id))
			return false;
		$this->id = $id;
		return true;
	}

}

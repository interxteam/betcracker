<?php

namespace Models;

/**
 * Description of kursyOu
 *
 * @author InterX-PC
 */
class Kraje {

	const tabela = 'kraj';

	protected
			$db,
			$id = null
	;

	public function __construct(\Library\Database\PdoWrapper $db) {
		$this->db = $db;
	}

	public function dodaj($nazwa) {

		if (!\Library\Validator::isSaveString($nazwa))
			return false;

		$czyZnalazl = $this->db->selectFirst(self::tabela, [
			'nazwa' => $nazwa,
		]);

		if (isset($czyZnalazl['id'])) {
			return $this->id = $czyZnalazl['id'];
		}

		$this->id = $this->db->insert(self::tabela, [
			'nazwa' => $nazwa,
		]);

		return $this->id;
	}
	
	public function pobierzDane() {
		if(\Library\Validator::isNull($this->id))
			return false;
		return  $this->db->selectFirst(self::tabela, ['id'=>  $this->id]);		
	}	

	public function pobierzWszystko() {
		return $this->db->select(self::tabela, null, null, null, ['Nazwa'=>'ASC']);
	}

	public function ustawIdMeczu($id) {
		if (!\Library\Validator::isNumber($id))
			return false;
		$this->idMeczu = $id;
		return true;
	}

	public function ustawId($id) {
		if (!\Library\Validator::isNumber($id))
			return false;
		$this->id = $id;
		return true;
	}

}

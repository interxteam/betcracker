<?php

namespace Models;

use Library\Database\PdoWrapper;
use Library\Validator;

class Uzytkownicy
{
    const table = 'uzytkownicy';
   
    protected static $statuses = array(
        'aktywny' => array('name'=>'aktywny'),
        'nieaktywny' => array('name'=>'nieaktywny'),
    );
	
    protected
        $db = null,
        $status = 'aktywny',
		$salt = '8e29c3c7fff0a4be221da60ae77ab2c88cbde178',
        $id = null
    ;

    public function __construct($db)
    {
        $this->db = $db;
		if(self::getLoggedUserId())
		{
			$this->id = self::getLoggedUserId();
		}
    }
    
    public function setId($id)
    {
        if(!is_numeric($id))
            return false;
        $this->id = $id;
        return true;
    }

    public function pobierzDane()
    {
        if(is_null($this->id))
            return false;
        
        $result = $this->db->select(self::table, array('id' => $this->id));
        if($result===false)
            return false;
        return $result[0];
    }
    
    public function login($email, $password)
    {
        $result = $this->db->selectFirst(self::table, array('email' => $email, 'password' => sha1($this->salt.$password), 'status' => 'active'));
        if($result===false)
            return false;
        
        $this->id = $result['id'];
        $_SESSION['model']['uzytkownicy']['logged-user'] = $result;
        $_SESSION['model']['uzytkownicy']['logged-user']['ttl'] = time()+300;
        
        return $result;
    }
    

    public static function isLogged()
    {
        return isset($_SESSION['model']['uzytkownicy']['logged-user']['id']);
    }
    
    public static function getLoggedUser()
    {
        if( isset($_SESSION['model']['uzytkownicy']['logged-user']) )
		{
            return $_SESSION['model']['uzytkownicy']['logged-user'];
		}
        return false;
    }
	
	public static function checkLoggedUserData()
	{
		if( isset($_SESSION['model']['uzytkownicy']['logged-user']['id']) )
		{
				self::$isUserDataChecked = true;
		}
	}
    
    public static function getLoggedUserId()
    {
        if( isset($_SESSION['model']['uzytkownicy']['logged-user']['id']) )
            return $_SESSION['model']['uzytkownicy']['logged-user']['id'];
        
        return false;
    }
    
    public static function logout()
    {
        session_unset();
    }
    
}
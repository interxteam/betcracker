<?php

namespace Models;

/**
 * Description of druzyna
 *
 * @author InterX-PC
 */
class Druzyny {
	
	const tabela = 'druzyna';
	const tabelaAlternatywnychNazw = 'druzyna_alt_nazwa';
	
	protected $db, $idKraju = null,$id=null, $serwisy = [
		'betexplorer.com' => [
			'name' => 'betexplorer.com',
		],
		'livescores.com' => [
			'name' => 'betexplorer.com',
		],
	];
	
	public function ustawIdKraju($id){
		$this->idKraju = $id;
	}

	public function __construct(\Library\Database\PdoWrapper $db) {
		$this->db = $db;
	}
	
	public function dodaj($nazwa, $serwis)
	{
		if( !isset($this->serwisy[$serwis]) )
			return false;
		
		if(is_null($this->idKraju))
			return null;
		
		$druzyna = $this->db->selectFirst(self::tabelaAlternatywnychNazw, [
			'u_kogo' => $serwis,
			'nazwa' => $nazwa,
		]);
		
		if($druzyna['id_druzyny'] > 0)
		{
			$this->id = $druzyna['id_druzyny'];
			return false;
		}
		
		$this->id = $this->db->insert(self::tabela, [
			'id_kraju' => $this->idKraju,
			'nazwa' => $nazwa,
		]);
		
		
		
		$this->db->insert(self::tabelaAlternatywnychNazw, [
			'id_druzyny' => $this->id,
			'u_kogo' => $serwis,
			'nazwa' => $nazwa,
		]);
		
		return $this->id;
	}
	
	public function pobierzId() {
		return $this->id;
	}
}

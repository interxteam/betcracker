<?php

namespace Models\Parsery;

class Betexplorer
{
	
	public function __construct($urlBuffor)
	{
		$this->urls_buffor = $urlBuffor;
	}
	
	public function pobierzMecze($url)
	{
	
		$html = $this->urls_buffor->getContent($url, $folderName);

		if(strpos($html, 'class="list-tabs__item__in">Main</a>') !== false)
		{
			$menuHtml = \Library\Functions::tnijTekst($html, '<div class="box-overflow" id="sm-0-0">', '</div>' );
			$koncowkaUrlDoWszystkichStatystyk = \Library\Functions::tnijTekst($menuHtml, '<a href="', '"' );
			$html = $this->urls_buffor->getContent($url.$koncowkaUrlDoWszystkichStatystyk, $folderName);
		}

		$rundy = \Library\Functions::tnijTekst($html, '<table class="table-main', '</table>' );


		$statystykiMeczy = array();
		$mecze = array();
		foreach(explode('</tr>', $rundy) as $runda)
		{
			if( strpos($runda, '<th') !== false)
			{
				continue;
			}
			$mecze[] = $runda;
		}
		unset($mecze[count($mecze)-1]);

	//	print_r($mecze);
	//	exit();

		$statystykiMeczy = [];
		foreach($mecze as $mecz)
		{
			$id = trim(\Library\Functions::tnijTekst($mecz, '<a href="', '"'));
			$id = substr(substr($id, strrpos($id, '/', -11)+1),0,-1);

			$data = date('Y-m-d', strtotime(trim(\Library\Functions::tnijTekst($mecz, '<td class="h-text-right h-text-no-wrap">', '</td>'))));
//			echo $data;

			$druzyny = trim(\Library\Functions::tnijTekst($mecz, 'class="in-match">', '</a>'));
			list($gospodarze, $goscie) = explode('-', $druzyny);
			$gospodarze = trim(\Library\Functions::tnijTekst($gospodarze, '<span>', '</span>'));
			$goscie = trim(\Library\Functions::tnijTekst($goscie, '<span>', '</span>'));

			$wynik = trim(\Library\Functions::tnijTekst($mecz, '<td class="h-text-center">', '</td>'));
			$wynik = trim(\Library\Functions::tnijTekst($wynik, '>', '<'));


			$statystykiMeczy[] = array(
				'id' => $id,
				'idBezNazwy' => substr($id,strpos($id,'/')+1),
				'date' => $data,
				'gospodarze' => $gospodarze,
				'goscie' => $goscie,
				'wynik' => $wynik,
		//		'HT' => $ht,
			);
		}
		return $statystykiMeczy;
	}
	
	public function pobierzSzczegolyMeczu($url)
	{
		$folderName = str_replace('/','-',substr($url, 34, strpos($url, '/',strpos($url, '/',35)+1)-34));
		
		$html = $this->urls_buffor->getContent($url, $folderName);
	//	$mecz = $obMecze->pobierzDanePrzezIdStatystyk($idStatystyk);

		if( $mecz === false)
		{
	//		$obMecze->dodaj();
		}

	//	echo $html;
		$wynik = Library\Functions::tnijTekst($html, '<ul class="list-details">','</ul>');
		$full = trim(Library\Functions::tnijTekst($wynik, 'id="js-score">','</p>'));
		$wynik = substr(trim(Library\Functions::tnijTekst($wynik, 'id="js-partial">','</h2>')),1,-1);

		$wynikDogrywki = null;
		$wynikKarnych = null;
		$iloscWynikow = substr_count($wynik, ',');
		
		if($iloscWynikow == 3)
		{
			list($ht, $drugaPolowa, $wynikDogrywki, $wynikKarnych) = explode(', ', $wynik);
		} elseif($iloscWynikow == 3)
		{
			list($ht, $drugaPolowa, $wynikDogrywki) = explode(', ', $wynik);
		} else
		{
			list($ht, $drugaPolowa) = explode(', ', $wynik);
		}
			
		var_dump('http://www.betexplorer.com/gres/ajax/matchodds.php?p=1&e='.$statystykiMeczy[$i]['idBezNazwy'].'&b=1x2');
		$html = json_decode($this->urls_buffor->getContent('http://www.betexplorer.com/gres/ajax/matchodds.php?p=1&e='.$statystykiMeczy[$i]['idBezNazwy'].'&b=1x2', $folderName, $url))->odds;

		$bukiDla1x2 = explode('</tr>', Library\Functions::tnijTekst($html, '<tbody>','</tbody>'));
		$kursy = array();

		foreach($bukiDla1x2 as $buk)
		{
			if( strpos($buk, 'http://www.bet365.com') !== false)
			{
				$kursy['1x2'] = Library\Functions::tnijTekstNaTablice($buk, 'data-odd="', '"');
				break;
			}
		}
//		var_dump('http://www.betexplorer.com/gres/ajax/matchodds.php?p=1&e='.$statystykiMeczy[$i]['id'].'&b=ou');
		$html = json_decode($this->urls_buffor->getContent('http://www.betexplorer.com/gres/ajax/matchodds.php?p=1&e='.$statystykiMeczy[$i]['idBezNazwy'].'&b=ou', $folderName, $url))->odds;
//		echo $html;
//		exit();
		$tabeleDlaOu = Library\Functions::tnijTekstNaTablice($html, '<tr ','</tr>');

		foreach($tabeleDlaOu as $tabela)
		{
			$tmp = $tabela;
			if( strpos($tmp,'bet365') === false )
				continue;

			$kursy['ou'][Library\Functions::tnijTekst($tmp,'<td class="table-main__doubleparameter">','</td>')] = Library\Functions::tnijTekstNaTablice($tmp, 'data-odd="', '"');
		}

		$html = json_decode($this->urls_buffor->getContent('http://www.betexplorer.com/gres/ajax/matchodds.php?p=1&e='.$statystykiMeczy[$i]['idBezNazwy'].'&b=ha', $folderName, $url))->odds;
		$kursy['ha'] = Library\Functions::tnijTekstNaTablice(Library\Functions::tnijTekst(Library\Functions::tnijTekst($html, '<tbody>','</tbody>'), 'http://www.bet365.com', '</tr>'), 'data-odd="', '"');

		$html = json_decode($this->urls_buffor->getContent('http://www.betexplorer.com/gres/ajax/matchodds.php?p=1&e='.$statystykiMeczy[$i]['idBezNazwy'].'&b=dc', $folderName, $url))->odds;
		$kursy['dc'] = Library\Functions::tnijTekstNaTablice(Library\Functions::tnijTekst(Library\Functions::tnijTekst($html, '<tbody>','</tbody>'), 'http://www.bet365.com', '</tr>'), 'data-odd="', '"');
		$statystykiMeczy[$i]['ht'] = $ht;
		$statystykiMeczy[$i]['kursy'] = $kursy;
	}
	
	
	public function pobierzSezony($url)
	{
		
	}
	
	private $urls_buffor =null;
}



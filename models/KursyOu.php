<?php
namespace Models;

/**
 * Description of kursyOu
 *
 * @author InterX-PC
 */
class KursyOu {
	const tabela = 'kursy_ou';
	
	protected 
		$db, 
		$idMeczu = null, 
		$id = null,
		$typ = '0.5',
		$typy = [
			'0.5' => ['nazwa' => '0.5'],
			'1.5' => ['nazwa' => '1.5'],
			'2.5' => ['nazwa' => '2.5'],
			'3.5' => ['nazwa' => '3.5'],
			'4.5' => ['nazwa' => '4.5'],
			'5.5' => ['nazwa' => '5.5'],
			'6.5' => ['nazwa' => '6.5'],
			'7.5' => ['nazwa' => '7.5'],
			'8.5' => ['nazwa' => '8.5'],
		]
	;
	
	public function __construct(\Library\Database\PdoWrapper $db) {
		$this->db = $db;
	}
	
	public function dodaj($stawkaOver, $stawkaUnder) {
		if(\Library\Validator::isNull($this->idMeczu))
			return false;
		if(!\Library\Validator::isNumber($stawkaOver))
			return false;
		if(!\Library\Validator::isNumber($stawkaUnder))
			return false;
		
		//spr czy nie istnieje
		$czyZnalazl = $this->db->selectFirst(self::tabela,[
			'id_meczu' => $this->idMeczu,
			'typ' => $this->typ,
		]);
		
		if(isset($czyZnalazl['id'])){
			return $this->id = $czyZnalazl['id'];
		}
			
		$this->id = $this->db->insert(self::tabela,[
			'id_meczu' => $this->idMeczu,
			'typ' => (string)$this->typ,
			'over' => $stawkaOver,
			'under' => $stawkaUnder,
		]);
		
		return $this->id;
	}
	
	public function sprawdzWygrana(){
		
		if(!\Library\Validator::isNull($this->idMeczu)) {
			$info = $this->db->selectFirst(self::tabela,[
				'id_meczu' => $this->idMeczu,
				'typ' => $this->typ,
			]);
			
		} elseif( !\Library\Validator::isNull($this->id) ) {
			$info = $this->db->selectFirst(self::tabela,[
				'id' => $this->id,
				'typ' => $this->typ,
			]);
		} else {
			return false;
		}
		
		if(!isset($info['id']))
			return false;
		
		$mecz = $this->db->selectFirst(\Models\Mecze::tabela,[
			'id' => $info['id_meczu'],
			'status' => 'zakonczono',
		]);
		
		list($goleGospodarzy, $goleGosci) = explode(':', $mecz['wynik']);
		
		if($goleGospodarzy == '-')
			return false;
		
		if( (float)$info['typ'] < $goleGospodarzy + $goleGosci){
			
			$wygrana = 'over';
			
		} else {
			$wygrana = 'under';
		}
			
		$this->db->update(self::tabela, [
			'wygrana' => $wygrana,
		], ['id' => $info['id']]);
		
		return true;
	}
	
	public function pobierzDaneDlaMeczu() {
		if(\Library\Validator::isNull($this->idMeczu))
			return false;
		
		$wynik = [];
		$kursy = $this->db->select(self::tabela,['id_meczu' => $this->idMeczu]);
		for($i=0,$lenI=count($kursy);$i<$lenI;$i++) {
			$wynik[$kursy[$i]['typ']] = $kursy[$i];
		}
		return $wynik;
	}
	
	public function pobierzTypy() {
		return $this->typy;
	}
	
	public function ustawTyp($typ) {
		
		if( !isset($this->typy[(string)$typ]) )
			return false;
		$this->typ = $typ;
		return true;
	}
	
	public function ustawIdMeczu($id) {
		if(!\Library\Validator::isNumber($id))
			return false;
		$this->idMeczu = $id;
		return true;
	}
	
	public function ustawId($id) {
		if(!\Library\Validator::isNumber($id))
			return false;
		$this->id = $id;
		return true;
	}
}

<?php

namespace Models;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Czas
 *
 * @author InterX-PC
 */
class Czas
{
	
	
	static public function pobierz()
	{
		if(isset($_SESSION['model']['Czas']['wartosc']))
			return $_SESSION['model']['Czas']['wartosc'];
		return date('Y-m-d');
	}
	
	static public function ustaw($date)
	{
		if(!Library\Validator::isDate($date))
			return false;
		$_SESSION['model']['Czas']['wartosc'] = $date;
		return true;
	}
	
}

<?php

namespace Models;


class Ligi {

	const tabela = 'liga';

	protected
			$db,
			$idKraju = null,
			$id = null
	;

	public function __construct(\Library\Database\PdoWrapper $db) {
		$this->db = $db;
	}

	public function dodaj($nazwa) {
		if(\Library\Validator::isNull($this->idKraju))
			return false;
		if (!\Library\Validator::isSaveString($nazwa))
			return false;
		
		$nazwa = trim($nazwa);

		$czyZnalazl = $this->db->selectFirst(self::tabela, [
			'id_kraju' => $this->idKraju,
			'nazwa' => $nazwa,
		]);

		if (isset($czyZnalazl['id'])) {
			return $this->id = $czyZnalazl['id'];
		}

		$this->id = $this->db->insert(self::tabela, [
			'id_kraju' => $this->idKraju,
			'nazwa' => $nazwa,
		]);

		return $this->id;
	}
	
	public function ustawIdPoNazwie($nazwa) {
		if(\Library\Validator::isNull($this->idKraju)) {
			return false;
		}
		if (!\Library\Validator::isSaveString($nazwa)) {
			return false;
		}
		$ele =  $this->db->selectFirst(self::tabela, ['id_kraju'=>  $this->idKraju, 'nazwa' => $nazwa]);
		
		if($ele)
		{
			$this->id = $ele['id'];
			return $this->id;
		}
		return false;
	}
		
	public function pobierzDane() {
		if(\Library\Validator::isNull($this->id))
			return false;
		return  $this->db->selectFirst(self::tabela, ['id'=>  $this->id]);		
	}	

	public function pobierzAbsolutnieWszystko() {
		return $this->db->select(self::tabela);
	}
	public function pobierzWszystko() {
		return $this->db->select(self::tabela, ['id_kraju' => $this->idKraju], null, null, ['Nazwa'=>'DESC']);
	}

	public function ustawIdKraju($id) {
		if (!\Library\Validator::isNumber($id))
			return false;
		$this->idKraju = $id;
		return true;
	}

	public function ustawId($id) {
		if (!\Library\Validator::isNumber($id))
			return false;
		$this->id = $id;
		return true;
	}

}

<?php

namespace Models;
/**
 * Description of kursyDc
 *
 * @author InterX-PC
 */
class KursyDc {
	
	const tabela = 'kursy_dc';
	
	protected 
		$db, 
		$idMeczu = null, 
		$id = null
	;
	
	public function __construct(\Library\Database\PdoWrapper $db) {
		$this->db = $db;
	}
	
	public function dodaj($stawka1x, $stawka12, $stawkaX2) {
		if(\Library\Validator::isNull($this->idMeczu))
			return false;
		if(!\Library\Validator::isNumber($stawka1x))
			return false;
		if(!\Library\Validator::isNumber($stawka12))
			return false;
		if(!\Library\Validator::isNumber($stawkaX2))
			return false;
		
		//spr czy nie istnieje
		$czyZnalazl = $this->db->selectFirst(self::tabela,[
			'id_meczu' => $this->idMeczu,
		]);
		
		if(isset($czyZnalazl['id'])){
			return $this->id = $czyZnalazl['id'];
		}
			
		$this->id = $this->db->insert(self::tabela,[
			'id_meczu' => $this->idMeczu,
			'1x' => $stawka1x,
			'12' => $stawka12,
			'x2' => $stawkaX2,
		]);
		
		return $this->id;
	}
	
	public function sprawdzWygrana(){
		
		if(!\Library\Validator::isNull($this->idMeczu)) {
			$info = $this->db->selectFirst(self::tabela,[
				'id_meczu' => $this->idMeczu,
			]);
			
		} elseif( !\Library\Validator::isNull($this->id) ) {
			$info = $this->db->selectFirst(self::tabela,[
				'id' => $this->id,
			]);
		} else {
			return false;
		}
		
		if(!isset($info['id']))
			return false;
		
		$mecz = $this->db->selectFirst(\Models\Mecze::tabela,[
			'id' => $info['id_meczu'],
			'status' => 'zakonczono',
		]);
		
		list($goleGospodarzy, $goleGosci) = explode(':', $mecz['wynik']);
		
		if($goleGospodarzy == '-')
			return false;
		
		if( (int)$goleGospodarzy > (int)$goleGosci){
			
			$wygrana1 = '1x';
			$wygrana2 = '12';
			
		} elseif ((int)$goleGospodarzy == (int)$goleGosci) {
			$wygrana1 = '1x';
			$wygrana2 = 'x2';
			
		} else {
			$wygrana1 = '12';
			$wygrana2 = 'x2';
		}
			
		$this->db->update(self::tabela, [
			'wygrana1' => $wygrana1,
			'wygrana2' => $wygrana2,
		], ['id' => $info['id']]);
		
		return true;
	}
	
	public function pobierzDaneDlaMeczu() {
		if(\Library\Validator::isNull($this->idMeczu))
			return false;
		
		return $this->db->selectFirst(self::tabela,['id_meczu' => $this->idMeczu]);
	}
	
	public function ustawIdMeczu($id) {
		if(!\Library\Validator::isNumber($id))
			return false;
		$this->idMeczu = $id;
		return true;
	}
	
	public function ustawId($id) {
		if(!\Library\Validator::isNumber($id))
			return false;
		$this->id = $id;
		return true;
	}
}

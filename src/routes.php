<?php
// Routes

$app->map(['GET', 'POST'],'/[{controller}[/{action}[/{id}[/{index}]]]]', function ($request, $response, $args) {
    // Sample log message
	
    $viewData = array(
//        'app' => &$app,
        'args' => $args,
		'router' => $this->router,
    );
	
	$controller = 'index';
	$action = 'index';
	if(isset($args['controller']) && !empty($args['controller']))
		$controller = $args['controller'];
	if(isset($args['action']) && !empty($args['action']))
		$action = $args['action'];
	
	if(!file_exists('../controllers/'.$controller.'/'.$action.'.php'))
		throw new Exception ('No action --->'.$controller.'/'.$action.' was found');
	
	include_once '../controllers/'.$controller.'/'.$action.'.php';
	
	
	if(isset($redirect)){
		return $redirect;
	} else
		return $this->view->render($response, $controller.'/'.$action.'.twig', $viewData);
})->setName('main');



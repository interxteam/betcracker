<?php

$container = $app->getContainer();


$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig('../views', [
        'cache' => '../tmp/views_cache',
		'auto_reload' => true,
    ]);
    $view->addExtension(new \Slim\Views\TwigExtension(
        $container['router'],
        $container['request']->getUri()
    ));

    return $view;
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], Monolog\Logger::DEBUG));
    return $logger;
};

$container['db'] = function ($c) {
    $config = $c->get('settings')['db'];
	$pdoWrapper = Library\Database\PdoWrapper::instance();
    $pdoWrapper->configMaster(
		$config['host'],
		$config['dbname'],
		$config['user'],
		$config['pass']
    );
	return $pdoWrapper;
};

$container['urls_buffor'] = function ($c) {
	return new Models\UrlsBuffor($c->get('settings')['paths']['buffor']['urls']);
};
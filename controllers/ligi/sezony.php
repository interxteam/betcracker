<?php

$obLigi = new Models\Ligi($this->db);
$obLigi->ustawId($args['id']);
$liga = $obLigi->pobierzDane();

$obKraje = new Models\Kraje($this->db);
$obKraje->ustawId($liga['id_kraju']);
$kraj = $obKraje->pobierzDane();


$obSezony = new Models\Sezony($this->db);
$obSezony->ustawIdLigi($liga['id']);

if(isset($_POST['data_od'])) {
	
	$obSezony->dodaj(
		$_POST['data_od'],
		$_POST['data_do'],
		$_POST['url']
	);
}

$sezony = $obSezony->pobierzWszystko();


$obKraje = new Models\Kraje($this->db);
$obKraje->ustawId($args['id']);

$viewData['liga'] = $liga;
$viewData['kraj'] = $kraj;
$viewData['sezony'] = $sezony;



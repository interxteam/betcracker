<?php

$obLigi = new Models\Ligi($this->db);
$obLigi->ustawIdKraju($args['id']);

if(isset($_POST['liga'])) {
	$obLigi->dodaj($_POST['liga']);
}

$ligi = $obLigi->pobierzWszystko();


$obKraje = new Models\Kraje($this->db);
$obKraje->ustawId($args['id']);

$viewData['ligi'] = $ligi;
$viewData['kraj'] = $obKraje->pobierzDane();;



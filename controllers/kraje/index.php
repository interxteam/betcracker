<?php

$obKraje = new Models\Kraje($this->db);

if(isset($_POST['kraj'])) {
	$obKraje->dodaj($_POST['kraj']);
}

$kraje = $obKraje->pobierzWszystko();

$viewData['kraje'] = $kraje;

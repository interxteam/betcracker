<?php

$wspoczynnikZysku = array();

$obLigi = new Models\Ligi($this->db);
$ligi = $obLigi->pobierzAbsolutnieWszystko();
//$obSezony = new Models\Sezony($this->db);
//$ligi = $obSezony->pobierzAbsolutnieWszystko();

//print_r($ligi);
//exit();

echo '<table>';
foreach($ligi as $liga) :
//	if($liga['zakonczone_pobieranie'] != 1)
//				continue;
	
$tab = $this->db->query('
	SELECT 
	t1.wynik,
		t2.gospodarze kurs1, 
		t2.remis kursX, 
		t2.goscie kurs2,
		t2.wygrana wygrana1x2,
		t3.gospodarze kurs1bez,
		t3.goscie kurs2bez,
		t3.wygrana wygranaHa
	FROM 
		`'.\Models\Mecze::tabela.'` t1 
	inner join 
		('.\Models\Kursy1x2::tabela.' t2,
		'.\Models\KursyHa::tabela.' t3 )
	ON 
	(
		t1.id = t2.id_meczu AND
		t1.id = t3.id_meczu
	)
	where
	id_ligi = '.$liga['id'].'
--	id_sezonu = '.$liga['id'].'
--	id_sezonu = 3
	order by 
		t2.gospodarze
');


//print_r($tab);
//exit();

echo '<tr><td style="vertical-align:top;background:#ccc">'.$liga['nazwa'].'</td>';

$mnozenieWRazieNiepowodzenia = 3;


for($przedzial=1.1;$przedzial<4;$przedzial+=0.2) {
	
	echo '<td>';
	echo '<b>Przedział '.$przedzial.' - '.($przedzial+0.2).'</b><br><br>';
	
	$wygrane = 0;
	$przegrane = 0;
	$zaden = 0;
	$bilans = 0.0;
	$wszystkie=0;
	$przegraneZRzedu=0.0;
	$wygraneZRzedu=0;

	$maxStrata = 0;

	for($i=0,$lenI=count($tab);$i<$lenI;$i++) {
		
		
		if($przedzial <= $tab[$i]['kurs1'] && $tab[$i]['kurs1'] < $przedzial+0.2) {
			/*
			$indexNastepnegoZakladuOOdpowiednimKursie = -1;
			for($j=$i+1;$j<$lenI;$j++)
				if($przedzial <= $tab[$j]['kurs1'] && $tab[$j]['kurs1'] < $przedzial+0.2) {
					$indexNastepnegoZakladuOOdpowiednimKursie = $j;
					break;
				}

			if($indexNastepnegoZakladuOOdpowiednimKursie === -1)
				break;
			
			$wszystkie++;
			if( $tab[$i]['wygranaHa'] == 'goscie' || $tab[$indexNastepnegoZakladuOOdpowiednimKursie]['wygranaHa'] == 'goscie') {
				$bilans -= 10.0*pow($mnozenieWRazieNiepowodzenia,$przegraneZRzedu);
				if( 10.0*pow($mnozenieWRazieNiepowodzenia,$przegraneZRzedu) > $maxStrata )
					$maxStrata = 10.0*pow($mnozenieWRazieNiepowodzenia,$przegraneZRzedu);
				$przegrane++;
				$przegraneZRzedu++;
			} elseif($tab[$i]['wygranaHa'] == 'gospodarze' && $tab[$indexNastepnegoZakladuOOdpowiednimKursie]['wygranaHa'] == 'zaden' ) {
				$bilans += $tab[$i]['kurs1bez']*10*pow($mnozenieWRazieNiepowodzenia,$przegraneZRzedu)-10*pow($mnozenieWRazieNiepowodzenia,$przegraneZRzedu);
				$przegraneZRzedu = 0;
				$wygrane++;
			} elseif($tab[$i]['wygranaHa'] == 'zaden' && $tab[$indexNastepnegoZakladuOOdpowiednimKursie]['wygranaHa'] == 'gospodarze' ) {
				$bilans += $tab[$indexNastepnegoZakladuOOdpowiednimKursie]['kurs1bez']*10*pow($mnozenieWRazieNiepowodzenia,$przegraneZRzedu)-10*pow($mnozenieWRazieNiepowodzenia,$przegraneZRzedu);
				$przegraneZRzedu = 0;
				$wygrane++;
			} else {
				$bilans += $tab[$i]['kurs1bez']*$tab[$indexNastepnegoZakladuOOdpowiednimKursie]['kurs1bez']*10*pow($mnozenieWRazieNiepowodzenia,$przegraneZRzedu)-10*pow($mnozenieWRazieNiepowodzenia,$przegraneZRzedu);
				$przegraneZRzedu = 0;
				$wygrane++;
			}
			$i = $indexNastepnegoZakladuOOdpowiednimKursie;
			*/
//			$obstawionaKwota = 10.0*pow($mnozenieWRazieNiepowodzenia,$przegraneZRzedu);
//			if($przegraneZRzedu > 2)
				$obstawionaKwota = 3.0*pow($mnozenieWRazieNiepowodzenia,$przegraneZRzedu);
//			else
//				$obstawionaKwota = 3.0*pow($mnozenieWRazieNiepowodzenia,5-$wygraneZRzedu);
			/*
			if( $tab[$i]['wygranaHa'] == 'goscie') {
				$bilans -= $obstawionaKwota;
				if( $obstawionaKwota > $maxStrata )
					$maxStrata = $obstawionaKwota;
				$przegrane++;
				$przegraneZRzedu++;
				$wygraneZRzedu=0;
			} elseif($tab[$i]['wygranaHa'] == 'zaden') {
				$zaden++;
			} else {
				$bilans += $tab[$i]['kurs1bez']*$obstawionaKwota-$obstawionaKwota;
				$przegraneZRzedu = 0;
				$wygrane++;
				$wygraneZRzedu++;
			}
			*/
			//*
			if( $tab[$i]['wygrana1x2'] == 'remis') {
				$bilans += $tab[$i]['kursX']*$obstawionaKwota-$obstawionaKwota;
				$przegraneZRzedu = 0;
				$wygrane++;
				$wygraneZRzedu++;
			} else {
				$bilans -= $obstawionaKwota;
				if( $obstawionaKwota > $maxStrata )
					$maxStrata = $obstawionaKwota;
				$przegrane++;
				$przegraneZRzedu++;
				$wygraneZRzedu=0;
			}
			
				$wszystkie++;
			 //*/
			/*
			if( $tab[$i]['wygrana1x2'] != 'gospodarze') {
				$bilans -= 10.0*pow(3,$przegraneZRzedu);
				if( 10.0*pow(3,$przegraneZRzedu) > $maxStrata )
					$maxStrata = 10.0*pow(3,$przegraneZRzedu);
				$przegrane++;
				$przegraneZRzedu++;
			} else {
				$bilans += $tab[$i]['kurs1bez']*10*pow(3,$przegraneZRzedu)-10*pow(3,$przegraneZRzedu);
				$przegraneZRzedu = 0;
				$wygrane++;

			}*/
		}
	}

//	echo 'Procent wygranych: '.round($wygrane/$wszystkie*100, 1).'%';
//	echo '<br>Procent niewaznych: '.round($zaden/$wszystkie*100, 1).'%';
//	echo '<br>Procent przegranych: '.round($przegrane/$wszystkie*100, 1).'%';
	echo '<br>Max strata: '.$maxStrata.'zł';
	var_dump($bilans, $wszystkie);
	
	if($wszystkie > 33)
	{
		if($maxStrata == 0)
			$maxStrata = 1;
		$wspoczynnikZysku[$bilans/$maxStrata] = ['nazwa'=>$liga['nazwa'],'maxstawka'=>$maxStrata];
	}
	echo '</td>';
}
echo '</tr>';


endforeach;

echo '</table>';

krsort($wspoczynnikZysku);
foreach($wspoczynnikZysku as $k => $v)
{
	echo $v['nazwa'].' - '.$k.' - '.$v['maxstawka'].'<br>';
}

exit();
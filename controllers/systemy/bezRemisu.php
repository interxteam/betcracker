<?php


$tab = $this->db->query('
	SELECT 
		t2.gospodarze kurs1, 
		t2.remis kursX, 
		t2.goscie kurs2,
		t2.wygrana wygrana1x2,
		t3.gospodarze kurs1bez,
		t3.goscie kurs2bez,
		t3.wygrana wygranaHa
	FROM 
		`'.\Models\Mecze::tabela.'` t1 
	inner join 
		('.\Models\Kursy1x2::tabela.' t2,
		'.\Models\KursyHa::tabela.' t3 )
	ON 
	(
		t1.id = t2.id_meczu AND
		t1.id = t3.id_meczu
	)
');


$kapital = 100;
$procentRyzykaKapitalu = 0.2;

$wygrane = 0;
$przegrane = 0;
$zaden = 0;
$bilans = 0.0;
$wszystkie=0;

$maxStrata = 0;

$mnozenieWRazieNiepowodzenia = 1;

$przegraneTeraz = 0;
for($i=0,$lenI=count($tab);$i<$lenI;$i++) {
	if(1.1 <= $tab[$i]['kurs1'] && $tab[$i]['kurs1'] < 1.4) {
		$wszystkie++;
		echo round($kapital,2).' zł - obstawiam '.round($procentRyzykaKapitalu*$kapital*pow($mnozenieWRazieNiepowodzenia,$przegraneTeraz),2) .'zł -  '.$tab[$i]['wygranaHa'].'<br>';
		if( $tab[$i]['wygranaHa'] == 'goscie') {
			$kapital -= $procentRyzykaKapitalu*$kapital*pow($mnozenieWRazieNiepowodzenia,$przegraneTeraz);
			if( $procentRyzykaKapitalu*$kapital*pow($mnozenieWRazieNiepowodzenia,$przegraneTeraz) > $maxStrata )
				$maxStrata = $procentRyzykaKapitalu*$kapital*pow($mnozenieWRazieNiepowodzenia,$przegraneTeraz);
			$przegrane++;
			$przegraneTeraz++;
		} elseif($tab[$i]['wygranaHa'] == 'zaden') {
			$zaden++;
		} else {
			$kapital += $tab[$i]['kurs1bez']*$procentRyzykaKapitalu*$kapital*pow($mnozenieWRazieNiepowodzenia,$przegraneTeraz)-$procentRyzykaKapitalu*$kapital*pow($mnozenieWRazieNiepowodzenia,$przegraneTeraz);
			$przegraneTeraz = 0;
			$wygrane++;
		}
	}
}

echo 'Procent wygranych: '.round($wygrane/$wszystkie*100, 1).'%';
echo '<br>Procent niewaznych: '.round($zaden/$wszystkie*100, 1).'%';
echo '<br>Procent przegranych: '.round($przegrane/$wszystkie*100, 1).'%';
echo '<br>Max strata: '.$maxStrata.'zł';


var_dump($kapital, count($tab));
exit();